<p align="center">
  <img with="100" height="100" src="https://github.com/goolhanrry/Navigator-plus-plus/blob/master/assets/Navigator-plus-plus_Icon.png" alt="icon">
</p>

<h1 align="center">Navigator++</h1>

<p align="center">
  <a href="https://www.travis-ci.org/goolhanrry/Navigator-plus-plus"><img src="https://www.travis-ci.org/goolhanrry/Navigator-plus-plus.svg?branch=master" alt="Build Status"></a>
  <a href="https://app.codacy.com/app/goolhanrry/Navigator-plus-plus?utm_source=github.com&utm_medium=referral&utm_content=goolhanrry/Navigator-plus-plus&utm_campaign=Badge_Grade_Dashboard"><img src="https://api.codacy.com/project/badge/Grade/4485ac973a3c4f1285f49f5c5ecb4065" alt="Codacy Badge"></a>
  <a href="LICENSE"><img src="https://img.shields.io/badge/license-MIT-blue.svg" alt="License"></a>
  <img src="https://img.shields.io/badge/platform-osx%20%7C%20win%20%7C%20linux-lightgrey.svg" alt="Platform">
  <br>
  <img width="624" src="https://github.com/goolhanrry/Navigator-plus-plus/blob/master/assets/Navigator-plus-plus_Screenshot1.png" alt="screenshot1">
  <img width="624" src="https://github.com/goolhanrry/Navigator-plus-plus/blob/master/assets/Navigator-plus-plus_Screenshot2.png" alt="screenshot2">
  <img width="624" src="https://github.com/goolhanrry/Navigator-plus-plus/blob/master/assets/Navigator-plus-plus_Screenshot3.png" alt="screenshot3">
</p>
